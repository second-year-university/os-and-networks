**Генератор масок подсети в 3 строчки на Python.**


- Элегантное решение:
```
for size in range(0, 33):
    byte = ((2 ** 32 - 1) << (32 - size)) % (2 ** 32)
    print('/%s - %s' % (size, '%s.%s.%s.%s' % (byte >> 24, (byte << 8) % (2**32) >> 24, (byte << 16) % (2**32) >> 24, (byte << 24) % (2**32) >> 24)))
```


- Короткое решение: 
```
for size in range(0, 33):
    print('/%s - %s' % (size, '%s.%s.%s.%s' % (((2 ** 32 - 1) << (32 - size)) % (2 ** 32) >> 24, (((2 ** 32 - 1) << (32 - size)) << 8) % (2**32) >> 24, (((2 ** 32 - 1) << (32 - size)) << 16) % (2**32) >> 24, (((2 ** 32 - 1) << (32 - size)) << 24) % (2**32) >> 24)))

```

- Пример вывода программы: 

```/0 - 0.0.0.0
/1 - 128.0.0.0
/2 - 192.0.0.0
/3 - 224.0.0.0
/4 - 240.0.0.0
/5 - 248.0.0.0
/6 - 252.0.0.0
/7 - 254.0.0.0
/8 - 255.0.0.0
/9 - 255.128.0.0
/10 - 255.192.0.0
/11 - 255.224.0.0
/12 - 255.240.0.0
/13 - 255.248.0.0
/14 - 255.252.0.0
/15 - 255.254.0.0
/16 - 255.255.0.0
/17 - 255.255.128.0
/18 - 255.255.192.0
/19 - 255.255.224.0
/20 - 255.255.240.0
/21 - 255.255.248.0
/22 - 255.255.252.0
/23 - 255.255.254.0
/24 - 255.255.255.0
/25 - 255.255.255.128
/26 - 255.255.255.192
/27 - 255.255.255.224
/28 - 255.255.255.240
/29 - 255.255.255.248
/30 - 255.255.255.252
/31 - 255.255.255.254
/32 - 255.255.255.255
```
