#!/usr/bin/python3

import os
import sys

if len(sys.argv) <= 1:
    print('Give a directory path as a command line argument')
    exit()

path = sys.argv[1]

files = os.listdir(path)

for file in files:
    f = open(path + '/' + file, 'r')
    f2 = open(path + '/' + file + '(wo comments)', 'w')
    for line in f:
        isComment = False
        for l in line:
            if l == ' ' or l == '\t':
                continue
            elif l == '#':
                isComment = True
                break
            else:
                break
        if not isComment:
            f2.write(line)
