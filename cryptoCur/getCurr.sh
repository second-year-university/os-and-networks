I=`dpkg -s jq 2> /dev/null | grep "Status"`

if [ -n "$I" ]
then
   echo "" > /dev/null
else
   echo "jq package is not installed!"
   echo "Run 'apt install jq' firstly!"
   exit
fi

link="https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms="
currency=(USD EUR RUB CNY UAH CZK SEK PLN JPY NOK)

for cur in  ${currency[@]}
do
    wget ${link}${cur} -q -O - | jq -r ".$cur" >  prices/${cur}.txt &
    wait
done

echo "Done!"
