**Практикум Docker - создание веб-приложения в контейнере**


- `Dockerfile` - конфигурация образа Docker
- `app.py` - приложение на Flask
- `templates/index.html` - шаблон web-страницы

[Демонстрация работы](http://s3.dolbak.ru:5000/)

Скриншот:

<img src="screen1.png" width="1000px">
