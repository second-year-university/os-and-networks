import base64

import RSA_UTIL


e, d, n = RSA_UTIL.gen_keys()
our_msg = bytes("""Блокировка публикаций. Роскомнадзор нередко выступает как инструмент массовой блокировки публикаций,
 затрагивающих честь и достоинство граждан и юридических лиц. Правовую основу для этого заложил закон, подписанный 23
  апреля 2018 Владимиром Путиным (№ 102-ФЗ "О внесении изменений в Федеральный закон «Об исполнительном производстве»
   и статью 15-1 Федерального закона «Об информации, информационных технологиях и о защите информации»). На практике 
   это позволило приставу самостоятельно обратиться в Роскомнадзор с заявкой о блокировке ресурса, если сайт, где
    расположена порочащая информация, по решению суда её не убирает[26].""", 'utf-8')
crypto = RSA_UTIL.enCode(e, n, base64.b64encode(our_msg))
print("ENCODED: ", crypto)
msg = RSA_UTIL.deCode(d, n, crypto)
print("DECODED: ", base64.b64decode(bytes(msg)).decode('utf-8'))

