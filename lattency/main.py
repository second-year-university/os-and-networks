from pythonping import ping
import matplotlib.pyplot as plt

PACKAGES = 2000

respsonse_list = ping('twitter.com', count=PACKAGES, size=56)

dataStr = str(respsonse_list).split('\r\n')

data = []


i = 0
for line in dataStr:
    if i == PACKAGES:
        break
    ms = line.split()[-1][:-2]
    try:
        data.append(float(ms))
    except Exception:
        print("Something is strange, ignoring")
    i += 1

f = open('output.txt', 'w')
for line in data:
    f.write(str(line) + '\n')
f.close()

fig, axs = plt.subplots(1, 1)
axs.hist(data, bins=100)
axs.set_title('Pinging twitter.com from Irkutsk, Russia. 2000 packages.')
plt.show()
