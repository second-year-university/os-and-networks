alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'

f = open('words.txt')
output = open('ready.txt', 'w', encoding='utf-8')
for line in f:
    newLine = ''
    for letter in line:
        if letter in alphabet:
            newLine += letter
    output.write(newLine + '\n')

output.close()