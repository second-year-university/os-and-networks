from moviepy.editor import *


def addAudio():
    videoclip = VideoFileClip("crops/cool.avi")
    audioclip = AudioFileClip("audio/music.mp3")

    new_audioclip = CompositeAudioClip([audioclip])
    videoclip.audio = new_audioclip
    videoclip.write_videofile("output/cool_video.mp4")
