import json
import re
from IP import SubNet

domains = set()

har = open('vk.com.har')
data = json.load(har)
har.close()

subNets = []
ipsFile = open('ips.txt')
for line in ipsFile:
    ip, subNetSize = line.split('/')
    subNetSize = int(subNetSize)
    subNets.append(SubNet(ip, subNetSize))
ipsFile.close()

for request in data['log']['entries']:
    ip = request['serverIPAddress']
    domain = re.search(r'https?://([A-Za-z_0-9.-]+).*', request['request']['url']).group(1)
    domains.add((ip, domain))

results = open('report.txt', 'w')

for domainTuple in domains:
    ip = domainTuple[0]
    domain = domainTuple[1]
    isListed = False
    if ip == '':
        results.write(domain + " _________NO IP!_________\n")
        continue
    for subNet in subNets:
        if subNet.isIpInSubnet(ip):
            isListed = True
    if isListed:
        results.write(ip + " " + domain + "\n")
    else:
        results.write(ip + " " + domain + " <<<<<<<<<NOT LISTED!>>>>>>>>>\n")
        print(domain)
results.close()